# Formula-E NodeMCU v1.8 #

## Instituto Tecnológico de Costa Rica ##
## Escuela de Ingeniería en Computadores ##
## CE1102 Taller de Programación, Grupo 2 ##
## Prof. Milton Villegas Lemus ##
## Proyecto III, I semestre 2019 ##
 
2019-06-07
Python 3.7.6

Ignacio Grané Rojas, 2019380056
Alejandro Soto Chacón, 2019008164

<https://gitlab.com/3442/FormulaE_CE_TEC>

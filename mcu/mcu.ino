/* Instituto Tecnológico de Costa Rica
 * Escuela de Ingeniería en Computadores
 * CE1102 Taller de Programación, Grupo 2
 * Prof. Milton Villegas Lemus
 * Proyecto II, I semestre 2019
 * 
 * Nombre del programa: Formula-E NodeMCU
 * Fecha de última modificación: 2019-05-02
 * Versión del lenguaje: C++14
 * Versión del programa: 1.5
 * País de producción: Costa Rica
 * 
 * Autores: Santiago Gamboa Ramírez
 *          Alejandro Soto Chacón, 2019008164
 *          Brian Wagemans Alvarado, 2019008628
 * 
 * Bibliotecas utilizadas: ESP8266 Arduino Core, versión 2.5.0.
 */

// Parte de ESP8266 Arduino Core. Provee servicios de red inalámbrica.
#include <ESP8266WiFi.h>

/* El ID de compilación se puede utilizar para diferenciar
 * distintos NodeMCUs durante depuración.
 */
#define ID_COMPILACION __DATE__ " " __TIME__

// Por requerimientos, se utilizan red y clave estáticas.
const char* const SSID_RED  = "WiFiCar";
const char* const CLAVE_RED = "pass1234";

// El servidor escucha en el puerto TCP 7070.
WiFiServer servidor(7070);
WiFiClient cliente;

#define PIN_B_2        D0
#define PIN_B_1        D1
#define PIN_A_2        D2
#define PIN_A_1        D3
#define PIN_A_PWM      D4
#define PIN_B_PWM      D5
#define PIN_BIT_LUZ    D8
#define PIN_RELOJ      D7
#define PIN_FOTOSENSOR D6
#define PIN_VOLTAJE    A0

bool     depurar              = false;
byte     estado_luces         = 0b00000000;
unsigned pwm_direccion        = 0;
unsigned pwm_traccion         = 0;
bool     signo_traccion       = true;
bool     signo_direccion      = true;
void   (*movimiento_actual)() = nullptr;

/* El significado de esta variable depende del movimiento complejo actual, pero
 * generalmente es un contador de la cantidad de 50ms transcurridos.
 */
int argumento_actual;

void actualizar_luces()
{
	shiftOut(PIN_BIT_LUZ, PIN_RELOJ, MSBFIRST, estado_luces);
}

void mover_motor(unsigned pin_a, unsigned pin_b, unsigned pin_pwm, bool signo, unsigned pwm)
{
	/* Entradas:      Pines directores y de señal de un mismo motor, dirección de rotación e intensidad.
	 * Restricciones: pin_a, pin_b, pin_pwm son pines; pwm <= 1023
	 * Procedimiento: Se comunica al L298 la acción.
	 */
	digitalWrite(pin_a, signo ? HIGH : LOW);
	digitalWrite(pin_b, signo ? LOW : HIGH);
	analogWrite(pin_pwm, pwm);
}

void mover_traccion(unsigned pwm)
{
	mover_motor(PIN_A_1, PIN_A_2, PIN_A_PWM, signo_traccion, pwm);
}

void mover_direccion(unsigned pwm)
{
	mover_motor(PIN_B_1, PIN_B_2, PIN_B_PWM, signo_direccion, pwm);
}

int leer_y_validar_entero(const String& cadena, int canario = 0xFAFA)
{
	/* Entradas:      Cadena a validar y valor canario opcional.
	 * Salidas:       Entero respectivo, o en su defecto el canario.
	 * Procedimiento: Se valida la entrada antes de tratar de convertirla.
	 * Restricciones: El entero a leer no debe superar cuatro dígitos decimales.
	 */

	int longitud = cadena.length();
	int corrimiento = longitud > 0 && cadena[0] == '-';
	longitud -= corrimiento;

	if(longitud == 0 && longitud > 4)
	{
		return canario;
	}

	for(int i = 0; i < longitud; ++i)
	{
		if(!isDigit(cadena[corrimiento + i]))
		{
			return canario;
		}
	}

	return cadena.toInt();
}

void detener()
{
	// Procedimiento: Finaliza cualquier movimiento actual del carro.

	signo_direccion = signo_traccion = true;
	pwm_direccion = pwm_traccion = 0;

	mover_traccion(0);
	mover_direccion(0);
}

void detener_complejo()
{
	// Procedimiento: Detiene solamente movimientos complejos, de haber.

	if(movimiento_actual != nullptr)
	{
		movimiento_actual = nullptr;
		detener();
	}
}

bool hacer_circular(int divisor)
{
	/* Salidas:       Indicación de si el círculo sigue en proceso.
	 * Procedimiento: Se aproxima un círculo en una dirección.
	 */

	if(argumento_actual > 300 / divisor)
	{
		// Se termina el movimiento al pasar 50ms*400 = 20s
		return false;
	} else if(argumento_actual == 0)
	{
		// En la primera ejecución se inicializa el movimiento.

		signo_direccion = !signo_direccion;

		signo_traccion = true;
		pwm_traccion = pwm_direccion = 1023;

		mover_traccion(pwm_traccion);
		mover_direccion(pwm_direccion);

		delay(250);

		signo_direccion = !signo_direccion;
		mover_direccion(pwm_direccion);
	}

	++argumento_actual;
	return true;
}

void movimiento_circle()
{
	// Procedimiento: Se hace un círculo en una sola dirección.

	if(!hacer_circular(1))
	{
		detener_complejo();
	}
}

void movimiento_infinite()
{
	// Procedimiento: Se hacen semicírculos y líneas rectas

	if(argumento_actual < 0)
	{
		if(argumento_actual == -30)
		{
			argumento_actual = 0;
			signo_direccion = !signo_direccion;
		} else
		{
			--argumento_actual;
		}
	} else if(!hacer_circular(2))
	{
		if(signo_direccion)
		{
			detener_complejo();
		} else
		{
			pwm_direccion = 0;
			argumento_actual = -1;
			mover_direccion(pwm_direccion);
		}
	}
}

void movimiento_zigzag()
{
	// Procedimiento: Se mueve en zig-zag.

	if(argumento_actual == -1)
	{
		signo_traccion = signo_direccion = true;
		pwm_direccion = 1023;
		pwm_traccion = 700;
	} else
	{
		bool sigue_cambio_direccion = true;
		int argumento = argumento_actual;
		unsigned ciclos = 0;

		while(argumento > 0 && ++ciclos < 10)
		{
			argumento -= sigue_cambio_direccion ? 5 : 20;
			sigue_cambio_direccion = !sigue_cambio_direccion;
		}

		if(ciclos >= 10)
		{
			detener_complejo();
		} else if(argumento == 0) // Se necesita cambiar la acción
		{
			if(sigue_cambio_direccion)
			{
				signo_direccion = !signo_direccion;

				mover_direccion(pwm_direccion);
				mover_traccion(pwm_traccion);
			} else
			{
				mover_direccion(0);
			}
		}
	}

	++argumento_actual;
}

void movimiento_especial()
{
	// Procedimiento: Dos semicírculos

	if(!hacer_circular(2))
	{
		if(signo_direccion)
		{
			detener_complejo();
		} else
		{
			signo_direccion = true;
			argumento_actual = 0;
		}
	}
}

String comando_pwm(const String& llave, const String& valor)
{
	/* Entradas:      Valores válidos en el rango [-1023, 1023].
	 * Procedimiento: Se altera la intensidad del motor de tracción.
	 */

	int pwm = leer_y_validar_entero(valor);

	bool signo = true;
	if(pwm < 0)
	{
		signo = false;
		pwm = -pwm;
	}

	if(pwm >= 1024)
	{
		return "out of range [-1023, 1023]: " + valor + ";";
	}

	detener_complejo();

	pwm_traccion = pwm;
	signo_traccion = signo;

	mover_traccion(pwm);
	if(pwm == 0)
	{
		mover_direccion(0);
	} else
	{
		mover_direccion(pwm_direccion);
	}

	return String();
}

String comando_dir(const String& llave, const String& valor)
{
	/* Entradas:      Valores aceptados {-1, 0, 1}
	 * Procedimiento: Se gira el motor de dirección.
	 */

	unsigned pwm = 1023;
	bool signo = true;

	switch(leer_y_validar_entero(valor))
	{
		case 1:
			break;

		case -1:
			signo = false;
			break;

		case 0:
			pwm = 0;
			break;

		default:
			return "Expected {-1, 0, 1};";
	}

	detener_complejo();

	pwm_direccion = pwm;
	signo_direccion = signo;

	mover_direccion(pwm);
	return String();
}

String comando_sense(const String& llave, const String& valor) {
	// Este comando no toma un valor. Lee el porcentaje de batería y la
	// luminosidad.

	/* Se normalizan rangos en la siguiente forma: 0-1023 -> 0-3.3V -> 0-8.15V
	 * -> 5.5V-6.0V -> 0%-100%
	 */
	unsigned porcentaje = (min(max(double(analogRead(PIN_VOLTAJE)), 640.0), 900.0) - 640.0) / 260.0 * 100;
	unsigned estado_luz = digitalRead(PIN_FOTOSENSOR);

	return "ok;" + String(porcentaje) + ";" + String(estado_luz) + ";";
}

String comando_luz(const String& llave, const String& valor)
{
	/* Entradas:      Llave de luz (o luces) y valor ("0" o "1")
	 * Procedimiento: Enciende o apaga las luces indicadas
	 */

	byte bits;
	switch(llave[1])
	{
		case 'f':
			bits = 0b00000011;
			break;

		case 'b':
			bits = 0b00001100;
			break;

		case 'l':
			bits = 0b00010000;
			break;

		case 'r':
			bits = 0b00100000;
			break;

		case 's':
			bits = 0b01000000;
			break;

		case 't':
			bits = 0b10000000;
			break;

		case 'a':
			bits = 0b11111111;
			break;

		default:
			return "unknown light: " + String(llave[1]) + ";";
	}

	switch(leer_y_validar_entero(valor))
	{
		case 0:
			estado_luces &= ~bits;
			break;

		case 1:
			estado_luces |= bits;
			break;

		default:
			return "invalid light state: " + valor + ";";
	}

	actualizar_luces();
	return String();
}

String comando_id(const String& llave, const String& valor)
{
	return "ok;" ID_COMPILACION ";";
}

String comando_debug(const String& llave, const String& valor)
{
	// Activa o desactiva el modo de depuración

	switch(leer_y_validar_entero(valor))
	{
		case 0:
			depurar = false;
			break;

		case 1:
			depurar = true;
			break;

		default:
			return "invalid debug state: " + valor + ";";
	}

	return String();
}

String comando_circle(const String& llave, const String& valor)
{
	switch(leer_y_validar_entero(valor))
	{
		case 1:
			signo_direccion = true;
			break;

		case -1:
			signo_direccion = false;
			break;

		default:
			return "Expected {-1, 1};";
	}

	argumento_actual = 0;
	movimiento_actual = &movimiento_circle;	

	return String();
}

String comando_infinite(const String& llave, const String& valor)
{
	signo_direccion = false;
	argumento_actual = 0;
	movimiento_actual = &movimiento_infinite;

	return String();
}

String comando_zigzag(const String& llave, const String& valor)
{
	argumento_actual = -1;
	movimiento_actual = &movimiento_zigzag;

	return String();
}

String comando_especial(const String& llave, const String& valor)
{
	signo_direccion = false;
	argumento_actual = 0;

	movimiento_actual = &movimiento_especial;

	return String();
}

String comando_stop(const String& llave, const String& valor)
{
	// Procedimiento: Detiene cualquier movimiento actual.

	detener();
	return String();
}

String comando_a0(const String& llave, const String& valor)
{
	// Obtiene el valor "puro" de la lectura del sensor de voltaje
	return "ok;" + String(analogRead(PIN_VOLTAJE)) + ";";
}

String ejecutar(const String& llave, const String& valor)
{
	/* Instituto Tecnológico de Costa Rica
	 * Escuela de Ingeniería en Computadores
	 * CE1102 Taller de Programación, Grupo 2
	 * Prof. Milton Villegas Lemus
	 * Proyecto II, I semestre 2019
	 * 
	 * Nombre del programa: Ejecución de un comando
	 * Fecha de última modificación: 2019-05-02
	 * Versión del lenguaje: C++14
	 * Versión del programa: 1.5
	 * País de producción: Costa Rica
	 * 
	 * Autores: Alejandro Soto Chacón, 2019008164
	 *          Brian Wagemans Alvarado, 2019008628
	 *
	 * Entradas:      Nombre del comando y valor proveido por el usuario.
	 * Procedimiento: Se asocia la llave a una función comando y se ejecuta.
	 */

	// Puntero a una función semejante a esta misma.
	String (*comando)(const String&, const String&) = nullptr;

	if(llave == "pwm")
	{
		comando = &comando_pwm;
	} else if(llave == "dir")
	{
		comando = &comando_dir;
	} else if(llave == "sense")
	{
		comando = &comando_sense;
	} else if(llave == "id")
	{
		comando = &comando_id;
	} else if(llave == "debug")
	{
		comando = &comando_debug;
	} else if(llave == "Circle")
	{
		comando = &comando_circle;
	} else if(llave == "Infinite")
	{
		comando = &comando_infinite;
	} else if(llave == "ZigZag")
	{
		comando = &comando_zigzag;
	} else if(llave == "especial")
	{
		comando = &comando_especial;
	} else if(llave == "stop")
	{
		comando = &comando_stop;
	} else if(llave == "a0")
	{
		comando = &comando_a0;
	} else if(llave[0] == 'l' && llave.length() == 2)
	{
		comando = &comando_luz;
	} else
	{
		return "undefined key value: " + llave + ";";
	}

	String respuesta = comando(llave, valor);
	if(respuesta.length() == 0)
	{
		// Respuesta predeterminada.
		respuesta = "ok;";
	}

	return respuesta;
}

void procesar(const String& linea)
{
	/* Entradas:      Línea de comando a procesar
	 * Procedimiento: Se divide la línea por separadores y se ejecuta cada comando.
	 */

	Serial.print(">> ");
	Serial.println(linea);
	Serial.print("<< ");

	int delimitador = 0, base = 0;
	while(delimitador >= 0 && base < linea.length())
	{
		String respuesta;

		// Se separan comandos en una misma línea por ';'
		delimitador = linea.indexOf(';', base);
		if(delimitador == -1)
		{
			respuesta = "missing delimiter;";
		} else
		{
			String llave, valor;

			// Se separan llaves de valores por ':'
			int indice = linea.indexOf(':', base);
			if(indice > 0 && indice < delimitador)
			{
				llave = linea.substring(base, indice);
				valor = linea.substring(indice + 1, delimitador);
			} else
			{
				llave = linea.substring(base, delimitador);
			}

			respuesta = ejecutar(llave, valor);
			base = delimitador + 1;
		}

		cliente.print(respuesta);
		Serial.print(respuesta);
	}

	cliente.println();
	Serial.println();
}

void loop()
{
	/* Instituto Tecnológico de Costa Rica
	 * Escuela de Ingeniería en Computadores
	 * CE1102 Taller de Programación, Grupo 2
	 * Prof. Milton Villegas Lemus
	 * Proyecto II, I semestre 2019
	 * 
	 * Nombre del programa: Bucle principal
	 * Fecha de última modificación: 2019-05-02
	 * Versión del lenguaje: C++14
	 * Versión del programa: 1.5
	 * País de producción: Costa Rica
	 * 
	 * Autores: Alejandro Soto Chacón, 2019008164
	 *          Brian Wagemans Alvarado, 2019008628
	 */

	// Se acepta un cliente a la vez, y solo si no hay uno activo.
	if(!cliente || !cliente.connected())
	{
		if(!servidor.hasClient())
		{
			return;
		} else if(cliente)
		{
			cliente.stop();
		}

		cliente = servidor.available();
		cliente.setNoDelay(true);
	}

	// Se procesa una línea de entrada del cliente, de existir.
	if(cliente.available())
	{
		String linea = cliente.readStringUntil('\n');

		int largo = linea.length();
		if(largo > 0 && linea[largo - 1] == '\r')
		{
			linea.remove(largo - 1);
		}

		procesar(linea);
	}

	/* Se altera la frecuencia de polling dependiendo de si hay actualmente
	 * algún movimiento complejo en ejecución.
	 */
	if(movimiento_actual != nullptr)
	{
		movimiento_actual();
		delay(50);
	} else
	{
		delay(500);
	}

	// En modo de depuración, las luces se utilizan para probar el fotosensor.
	if(depurar)
	{
		estado_luces = digitalRead(PIN_FOTOSENSOR) ? 0b00000000 : 0b11111111;
		actualizar_luces();
	}
}

bool conectar_a_red(const char* nombre, const char* clave)
{
	/* Entradas:      Nombre y clave de la red.
	 * Salidas:       Indicación del estado de conexión.
	 * Procedimiento: Se trata de conectar a la red.
	 */

	WiFi.begin(nombre, clave);
	while(true)
	{
		delay(250);

		switch(WiFi.status())
		{
			case WL_CONNECTED:
				return true;

			case WL_IDLE_STATUS:
			case WL_DISCONNECTED:
				break;

			default:
				return false;
		}
	}
}

void buscar_red()
{
	// Por requerimientos, se usa la dirección estática 192.168.43.200/24.
	IPAddress ip(172, 20, 10, 5);
	IPAddress puerta(172, 20, 10, 1);
	IPAddress subred(255, 255, 255, 240);
	WiFi.config(ip, puerta, subred);

	WiFi.mode(WIFI_STA);
	WiFi.disconnect();

	while(!conectar_a_red(SSID_RED, CLAVE_RED))
	{
		continue;
	}
}

void setup()
{
	/* Instituto Tecnológico de Costa Rica
	 * Escuela de Ingeniería en Computadores
	 * CE1102 Taller de Programación, Grupo 2
	 * Prof. Milton Villegas Lemus
	 * Proyecto II, I semestre 2019
	 * 
	 * Nombre del programa: Inicialización del NodeMCU
	 * Fecha de última modificación: 2019-05-02
	 * Versión del lenguaje: C++14
	 * Versión del programa: 1.5
	 * País de producción: Costa Rica
	 * 
	 * Autores: Alejandro Soto Chacón, 2019008164
	 *          Brian Wagemans Alvarado, 2019008628
	 */

	Serial.begin(115200);

	pinMode(PIN_A_1, OUTPUT);
	pinMode(PIN_A_2, OUTPUT);
	pinMode(PIN_B_1, OUTPUT);
	pinMode(PIN_B_2, OUTPUT);
	pinMode(PIN_A_PWM, OUTPUT);
	pinMode(PIN_B_PWM, OUTPUT);
	pinMode(PIN_RELOJ, OUTPUT);
	pinMode(PIN_BIT_LUZ, OUTPUT);
	pinMode(PIN_FOTOSENSOR, INPUT);
	pinMode(PIN_VOLTAJE, INPUT);

	/* Si no se escribe un valor inicial al registro de corrimiento, el mismo
	 * contendrá un valor indeterminado al arranque y por tanto parecerá como
	 * que algunas luces se encienden aleatoriamente.
	 */
	actualizar_luces();

	buscar_red();
	servidor.begin();
}

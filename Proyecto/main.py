# Para todo aquello referente a la interfaz gráfica
import pygame

# Para el retraso al clickear un botón
from time import sleep

# Fuentes de texto
import fonts

Clickeables = []
CuadroEdicion = None
Terminar = False

def salir():
    # Causa que se rompa el bucle principal
    global Terminar
    Terminar = True

def establecer_temporizador(Tiempo, Accion):
    """
    Entradas:      Periodo del temporizador (en segundos) y acción de respuesta
    Procedimiento: Se crea un timer de pygame y se registra la acción en el bucle principal
    """

    global AccionTemporizador

    pygame.time.set_timer(pygame.USEREVENT, int(Tiempo * 1000))
    AccionTemporizador = Accion

def quitar_temporizador():
    global AccionTemporizador

    # pygame considera un temporizador de intervalo cero como deshabilitado
    pygame.time.set_timer(pygame.USEREVENT, 0)
    AccionTemporizador = None

def registrar_boton(Area, Redibujar, Accion, ColorSuelto, ColorPresionado):
    # Esta primera llamada garantiza que el botón sea dibujado
    # la primera vez, antes de cualquier evento
    Redibujar(ColorSuelto)

    def al_presionar():
        # Se crea un efecto estético en el que se cambia el color
        # del botón y además se genera un retraso artificial antes
        # de ejecutar la acción de respuesta

        Redibujar(ColorPresionado)
        pygame.display.flip()
        
        sleep(0.2)

        Redibujar(ColorSuelto)
        pygame.display.flip()

        Accion()

    Clickeables.append((Area, al_presionar))

def cuadro_edicion(X, Y, Ancho, Alto, Contenedor, Fuente = fonts.default, ColorFondo = (255, 255, 255), ColorTexto = (0, 0, 0)):
    Area = pygame.Rect(X, Y, Ancho, Alto)

    def dibujar():
        Etiqueta = Fuente.render(Contenedor[0], 1, ColorTexto)

        AnchoEtiqueta, AltoEtiqueta = Etiqueta.get_size()

        Corrimiento = max(AnchoEtiqueta, Ancho) - Ancho
        YEtiqueta = Y + Alto // 2 - AltoEtiqueta // 2

        pygame.draw.rect(Pantalla, ColorFondo, Area, 0)
        Pantalla.blit(Etiqueta, (X, YEtiqueta), (Corrimiento, 0, min(AnchoEtiqueta, Ancho), AltoEtiqueta))

        pygame.display.flip()

    def al_escribir(Texto):
        if Texto == '\b':
            Contenedor[0] = Contenedor[0][:-1]
        elif Texto.isprintable():
            Contenedor[0] += Texto

        dibujar()

    def al_clickear():
        global CuadroEdicion
        CuadroEdicion = (Area, al_escribir)

    dibujar()
    Clickeables.append((Area, al_clickear))

def boton_textual(
    X, Y, Ancho, Alto, Texto, Accion, Fuente = fonts.default,
    ColorSuelto = (170, 170, 170), ColorPresionado = (140, 140, 140),
    ColorTexto = (0, 0, 0)
):
    """
    Entradas:      Posición, dimensiones, acción, fuentes, texto y colores de un nuevo botón
    Procedimiento: Se registra un nuevo botón con el texto, fuente y colores elegidos
    """

    Area = pygame.Rect(X, Y, Ancho, Alto)
    Etiqueta = Fuente.render(Texto, 1, ColorTexto)

    AnchoEtiqueta, AltoEtiqueta = Etiqueta.get_size()

    # Posición centrada con respecto al rectángulo contenedor
    PosicionEtiqueta = (X + Ancho // 2 - AnchoEtiqueta // 2, Y + Alto // 2 - AltoEtiqueta // 2)

    def redibujar(Color):
        # Como se redibuja el rectángulo del botón, es también necesario
        # redibujar el texto de etiqueta
        pygame.draw.rect(Pantalla, Color, Area, 0)
        Pantalla.blit(Etiqueta, PosicionEtiqueta)

    registrar_boton(Area, redibujar, Accion, ColorSuelto, ColorPresionado)

def cambiar_a(Vista):
    global RecibeTeclas

    # Se invalidan todos los botones anteriormente registrados
    Clickeables.clear()
    RecibeTeclas = None

    Vista.mostrar(Pantalla)

    # display.flip() hace que se muestre en pantalla el último
    # frame construido
    pygame.display.flip()

def recibir_teclas(Accion):
    global RecibeTeclas
    RecibeTeclas = Accion

def bucle_principal(Vista):
    """
    Instituto Tecnológico de Costa Rica
    Escuela de Ingeniería en Computadores
    CE1102 Taller de Programación, Grupo 2
    Prof. Milton Villegas Lemus
    Proyecto III, I semestre 2019
    
    Nombre del programa: Bucle principal de la aplicación
    Fecha de última modificación: 2019-06-04
    Versión del lenguaje: Python 3.7.6
    Versión del programa: 1.1
    País de producción: Costa Rica
    
    Autores: Ignacio Grané Rojas, 2019380056
             Alejandro Soto Chacón, 2019008164
    
    Bibliotecas utilizadas: PyGame 1.9.6

    Entradas:      Vista inicial
    Procedimiento: Se inicializa pygame y el bucle principal de manejo de eventos.
    """

    global Pantalla, CuadroEdicion

    # Inicializar todos los componentes de pygame que hayan sido importados
    pygame.init()

    # Se crea una ventana con las dimensiones indicadas
    Pantalla = pygame.display.set_mode((1536, 864))

    cambiar_a(Vista)

    # Bucle principal de la aplicación
    while not Terminar:
        for Evento in pygame.event.get():
            if Evento.type == pygame.QUIT:
                salir()
            elif Evento.type == pygame.MOUSEBUTTONDOWN:
                X, Y = pygame.mouse.get_pos()

                if CuadroEdicion is not None and not CuadroEdicion[0].collidepoint(X, Y):
                    CuadroEdicion = None

                for Area, Accion in Clickeables:
                    # Se activa el botón en cuestión si las coordenadas del click
                    # colisionan con su rectángulo contenedor
                    if Area.collidepoint(X, Y):
                        Accion()
                        break
            elif Evento.type == pygame.USEREVENT:
                AccionTemporizador()
            elif Evento.type == pygame.KEYDOWN:
                if CuadroEdicion is not None:
                    CuadroEdicion[1](Evento.unicode if Evento.key != pygame.K_BACKSPACE else '\b')
                elif RecibeTeclas is not None:
                    RecibeTeclas(Evento, True)
            elif Evento.type == pygame.KEYUP:
                if RecibeTeclas is not None:
                    RecibeTeclas(Evento, False)

    pygame.quit()

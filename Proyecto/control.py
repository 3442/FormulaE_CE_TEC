# Servicios de red
import socket

# Para os.environ
import os

DEFAULT_ADDRESS = os.environ.get('CAR_IP', '172.20.10.5')
DEFAULT_PORT    = 7070
DEFAULT_TIMEOUT = 3

# Comandos de luces
FRONT_LIGHTS       = 'lf'
BACK_LIGHTS        = 'lb'
FRONT_LEFT_LIGHTS  = 'll'
FRONT_RIGHT_LIGHTS = 'lr'
BACK_LEFT_LIGHTS   = 'ls'
BACK_RIGHT_LIGHTS  = 'lt'
ALL_LIGHTS         = 'la'
LIGHT_TYPES        = (FRONT_LIGHTS, BACK_LIGHTS, FRONT_LEFT_LIGHTS, FRONT_RIGHT_LIGHTS,
                      BACK_LEFT_LIGHTS, BACK_RIGHT_LIGHTS, ALL_LIGHTS)

# Constantes de dir
LEFT   = -1
CENTER =  0
RIGHT  =  1

Connection = None

def connect(Address = None, Port = None):
    """
    Entradas:      Dirección IP y puerto del servidor (None para usar valores predeterminados)
    Salidas:       Indicación de si la conexión tuvo éxito
    Procedimiento: Inicializa la conexión
    """

    global Connection

    Address = Address or DEFAULT_ADDRESS
    Port = Port or DEFAULT_PORT

    # Si hay una conexión previa, se cierra primero
    if Connection:
        Connection.close()

    # Se crea el socket
    Connection = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    Connection.settimeout(DEFAULT_TIMEOUT)

    try:
        Connection.connect((Address, Port))
        return True
    except (socket.timeout, ConnectionRefusedError):
        Connection.close()
        Connection = None
        return False

def close():
    """
    Procedimiento: Cierra la conexión, de existir
    """

    global Connection

    if Connection is not None:
        Connection.close()
        Connection = None

def execute(Command, Argument = None):
    """
    Instituto Tecnológico de Costa Rica
    Escuela de Ingeniería en Computadores
    CE1102 Taller de Programación, Grupo 2
    Prof. Milton Villegas Lemus
    Proyecto III, I semestre 2019
    
    Nombre del programa: Transmisión de comandos del cliente al servidor
    Fecha de última modificación: 2019-05-25
    Versión del lenguaje: Python 3.7.6
    Versión del programa: 1.1
    País de producción: Costa Rica
    
    Autores: Ignacio Grané Rojas, 2019380056
             Alejandro Soto Chacón, 2019008164

    Entradas:      Nombre del comando a ejecutar y su argumento (o None si no lo posee)
    Salidas:       False si ocurre un fallo, True si se responde con "ok;" o una lista de
                   mensajes de respuesta si la respuesta comienza con "ok;" pero contiene
                   más información.
    Procedimiento: Se codifica y envía el comando; se recibe y decodifica la respuesta.
    """

    global Connection

    # No se puede enviar un comando por una conexión que no existe
    if Connection is None:
        return False

    # Se genera la línea de comando
    Line = Command + ('' if Argument is None else ':' + str(Argument)) + ';\n'
    try:
        # Se envía por completo el mensaje, codificado en UTF-8
        Connection.sendall(bytes(Line, 'utf-8'))

        # Se recibe y concatena la respuesta en un bytearray hasta encontrar el
        # fin de la línea
        ResponseBytes = bytearray()
        while len(ResponseBytes) == 0 or ResponseBytes[-1] != ord(b'\n'):
            ResponseBytes += Connection.recv(256)
    except socket.timeout:
        # Hay fallo si ocurre un timeout
        close()
        return False

    # Se decodifica la respuesta, la cual debe terminar con un
    # punto y coma
    Response = ResponseBytes.decode('utf-8').strip()
    assert(Response[-1] == ';')

    # Luego se separan y validan los componentes de la respuesta

    Parts = Response.split(';')[:-1]

    assert(len(Parts) > 0)
    if Parts[0] != 'ok':
        raise Exception('Response failure: ' + Response)

    return True if len(Parts) == 1 else Parts[1:]

def set_power(Value):
    # Envía un valor de PWM indicado por el argumento

    assert type(Value) is int and -1023 <= Value <= 1023
    return execute('pwm', Value)

def set_direction(Direction):
    # Cambia el motor de dirección

    assert type(Direction) is int and Direction in (LEFT, CENTER, RIGHT)
    return execute('dir', Direction)

def read_sensors():
    # Lee los sensores de iluminación y de nivel de batería

    Response = execute('sense')
    assert type(Response) is list and len(Response) == 2

    return (int(Response[0]), bool(int(Response[1])))

def get_id():
    # Devuelve la fecha y hora de compilación del software del MCU

    Response = execute('id')
    assert type(Response) is list and len(Response) == 1

    return Response[0]

def read_battery_tension():
    # Toma una lectura del nivel de batería sin procesamiento alguno

    Response = execute('a0')
    assert type(Response) is list and len(Response) == 1

    return int(Response[0])

def set_debug(Enable):
    # Activa o desactiva el modo de depuración del MCU

    assert type(Enable) is bool
    return execute('debug', int(Enable))

def do_circle(Direction):
    # Realiza un movimiento en círculo en la dirección dada

    assert type(Direction) is int and Direction in (LEFT, RIGHT)
    return execute('Circle', Direction)

def set_lights(Lights, Enable):
    # Enciende o apaga las luces indicadas

    assert type(Lights) is str and Lights in LIGHT_TYPES and type(Enable) is bool
    return execute(Lights, int(Enable))

# Otros movimientos complejos
def do_infinite(): return execute('Infinite')
def do_zigzag():   return execute('ZigZag')
def do_special():  return execute('especial')

# Detiene todo movimiento en proceso
def stop(): return execute('stop')

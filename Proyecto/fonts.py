# Para construir las fuentes
import pygame

# Si no se inicializa aquí el subsistema de fuentes,
# ocurrirán execpciones ya que no se habrá llamado
# a pygame.init() en main.py todavía.
pygame.font.init()

# Fuentes utilizadas a lo largo de la aplicación
nombreAbout       = pygame.font.SysFont('Arial', 24)
atras             = pygame.font.SysFont('Blackadder ITC', 18)
tituloMenu        = pygame.font.SysFont('Elephant', 50)
temporadaMenu     = pygame.font.SysFont('Times New Roman', 25)
datosMenu         = pygame.font.SysFont('Bahnschrift', 50)
patrocinador      = pygame.font.SysFont('Bahnschrift', 25)
tituloTestDrive   = pygame.font.SysFont('Pristina', 75)
productoTestDrive = pygame.font.SysFont('Pristina', 40)
pilotoTestDrive   = pygame.font.SysFont('Pristina', 35)
categoriaTabla    = pygame.font.SysFont('Pristina', 40)
tituloTabla       = pygame.font.SysFont('Times New Roman', 20)
datosTabla        = pygame.font.SysFont('Times New Roman', 30)
pilotoTabla       = pygame.font.SysFont('Times New Roman', 20)
vehiculoTabla     = pygame.font.SysFont('Times New Roman', 25)

# Fuente predeterminada
default = nombreAbout

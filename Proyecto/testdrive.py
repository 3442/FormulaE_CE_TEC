#Bibliotecas
import time
import pygame
import traceback #Reportar excepciones

#Archivos
import main
import menu 
import fonts        #Fuentes
import testdrive    #Para refresar la vista
import control      #Para controlar el carro
import files        #Cargar imágenes

PitSeleccionado = 1
Conectado = False
Fallido = False
NivBateria = 100
NivLuz = False
Luces = {}
Potencia = 0
Direccion = control.CENTER
Aceleracion = 0
Contador = 0

def sleep_reload(Tiempo):
    main.cambiar_a(testdrive)
    time.sleep(Tiempo)

def tecla(Tecla, Presionada):
    global Aceleracion, Direccion

    K = Tecla.key
    if K == pygame.K_LEFT:
        if Presionada:
            mover_hacia(control.LEFT)
        else:
            mover_hacia(control.CENTER)
    elif K == pygame.K_RIGHT:
        if Presionada:
            mover_hacia(control.RIGHT)
        else:
            mover_hacia(control.CENTER)
    elif K == pygame.K_UP:
        Aceleracion = 50 if Presionada else 0
    elif K == pygame.K_DOWN:
        Aceleracion = -50 if Presionada else 0
    elif K == pygame.K_f and Presionada:
        frenar()
    elif K == pygame.K_l and Presionada:
        movimiento(celebrar)
    elif K == pygame.K_e and Presionada:
        movimiento(control.do_special)
    elif K == pygame.K_z and Presionada:
        movimiento(control.do_zigzag)
    elif K == pygame.K_c and Presionada:
        movimiento(lambda: control.do_circle(control.RIGHT))
    elif K == pygame.K_i and Presionada:
        movimiento(control.do_infinite)

def celebrar():
    global Potencia

    frenar()
    main.quitar_temporizador()

    if PitSeleccionado == 1:
        mover_hacia(control.LEFT)
        Potencia = 700
        control.set_power(Potencia)
        sleep_reload(3)
        control.set_power(0)
        mover_hacia(control.CENTER)
        Potencia = -700
        control.set_power(Potencia)
        sleep_reload(2)
        Potencia = 0
        control.set_power(0)
    else:
        control.set_power(1023)
        for _ in range(4):
            sleep_reload(4)
            mover_hacia(control.RIGHT)
            sleep_reload(2.5)
            mover_hacia(control.CENTER)

        control.set_lights(control.ALL_LIGHTS, True)
        for Type in control.LIGHT_TYPES:
            Luces[Type] = True

        sleep_reload(2)

        Luces.clear()
        control.set_lights(control.ALL_LIGHTS, False)

    main.establecer_temporizador(0.25, chequear_estado)
    main.cambiar_a(testdrive)

def intentar(AccionRed, AccionFinal = lambda _: None):
    # Trata de ejecutar AccionRed y pasar su resultado a
    # AccionFinal. Si AccionRed falla, pone el test drive
    # en estado de fallo y no ejecuta AccionFinal.

    global Fallido

    try:
        Resultado = AccionRed()
        if Resultado is False:
            Fallido = True
    except:
        traceback.print_exc()
        Fallido = True

    if Fallido:
        main.quitar_temporizador()
        main.cambiar_a(testdrive)
    else:
        AccionFinal(Resultado)

def chequear_estado():
    global Contador

    # Llamada periodicamente por el temporizador. Invoca
    # el comando sense para actualizar los indicadores de
    # sensores y para verificar el estado de la conexión.
    # Además, realiza acciones periódicas como parpadear
    # luces dadas ciertas condiciones.

    def actualizar(Sensores):
        global NivBateria, NivLuz, EstadoCarro

        NuevaBateria, NuevaLuz = Sensores
        if NuevaLuz != NivLuz and Luces.get(control.FRONT_LIGHTS, False) != NuevaLuz:
            cambiar_luces(control.FRONT_LIGHTS)

        NivBateria, NivLuz = NuevaBateria, NuevaLuz

        NuevoEstado = 'available' if NivBateria > 30 else 'discharged'
        if NuevoEstado != EstadoCarro:
            EstadoCarro = NuevoEstado
            files.Cars[files.LastYear]['status'] = NuevoEstado
            files.update_car(files.LastYear)

        intentar(lambda: main.cambiar_a(testdrive))
        if Potencia < 0:
            cambiar_luces(control.BACK_LIGHTS)

        if Direccion == control.LEFT:
            cambiar_luces(control.FRONT_LEFT_LIGHTS)
        elif Direccion == control.RIGHT:
            cambiar_luces(control.FRONT_RIGHT_LIGHTS)

        main.cambiar_a(testdrive)

    if (Contador & 7) == 0:
        intentar(control.read_sensors, actualizar)
        Contador = 0

    if (Contador & 1) == 0 and Aceleracion != 0:
        global Potencia

        NuevaPotencia = Potencia + Aceleracion
        if NuevaPotencia < -1023:
            NuevaPotencia = -1023
        elif NuevaPotencia > 1023:
            NuevaPotencia = 1023
        elif Potencia == 0 and NuevaPotencia > 0:
            NuevaPotencia = 500
        elif Potencia == 0 and NuevaPotencia < 0:
            NuevaPotencia = -500
        elif 0 < Potencia <= 500 and NuevaPotencia < 500:
            NuevaPotencia = 0
        elif -500 <= Potencia < 0 and NuevaPotencia > -500:
            NuevaPotencia = 0

        if NuevaPotencia != Potencia:
            Potencia = NuevaPotencia
            intentar(lambda: control.set_power(Potencia), lambda _: cambiar_luces(control.BACK_LIGHTS) if Luces.get(control.BACK_LIGHTS, False) else None)
            main.cambiar_a(testdrive)

    Contador += 1

def conectar():
    # Trata de conectarse al servidor y reporta su
    # estado de éxito a través de las variables globales
    # de test drive y la creación o no del temporizador.

    global Conectado, Fallido

    if control.connect():
        Conectado = True
        main.establecer_temporizador(0.25, chequear_estado)

        pygame.display.flip()
        chequear_estado()
    else:
        Conectado = False
        Fallido = True

        main.cambiar_a(testdrive)

def desconectar():
    # Finaliza la conexión y devuelve las variables
    # globales a sus estados predeterminados.

    global Conectado, Fallido, Luces, Potencia

    if Conectado:
        intentar(lambda: control.stop())

    control.close()
    main.quitar_temporizador()

    Fallido = False
    Conectado = False
    Luces = {}
    Potencia = 0
    Direccion = control.CENTER

def cambiar_luces(ID):
    # Invierte el estado de la luz o luces indicadas
    Estado = Luces.get(ID, False)

    def actualizar_diccionario(_):
        Luces[ID] = not Estado
        main.cambiar_a(testdrive)

    intentar(lambda: control.set_lights(ID, not Estado), actualizar_diccionario)

def frenar():
    # Frena el dispositivo
    global Potencia

    Potencia = 0
    intentar(lambda: control.stop(), lambda _: mover_hacia(control.CENTER))

def mover_hacia(Orientacion):
    # Cambia la orientación del movimiento

    global Direccion

    if Direccion != Orientacion:
        Direccion = Orientacion
        intentar(lambda: control.set_direction(Direccion))

        if Orientacion != control.LEFT and Luces.get(control.FRONT_LEFT_LIGHTS, False):
            cambiar_luces(control.FRONT_LEFT_LIGHTS)
   
        if Orientacion != control.RIGHT and Luces.get(control.FRONT_RIGHT_LIGHTS, False):
            cambiar_luces(control.FRONT_RIGHT_LIGHTS)

def movimiento(Accion):
    def actualizar(_):
        global Potencia
        global Direccion

        Potencia = 0
        Direccion = control.CENTER

        main.cambiar_a(testdrive)

    intentar(Accion, actualizar)

def mostrar(Pantalla):
    """
    Instituto Tecnológico de Costa Rica
    Escuela de Ingeniería en Computadores
    CE1102 Taller de Programación, Grupo 2
    Prof. Milton Villegas Lemus
    Proyecto III, I semestre 2019
    
    Nombre del programa: Vista de Test Drive
    Fecha de última modificación: 2019-06-03
    Versión del lenguaje: Python 3.7.6
    Versión del programa: 1.1
    País de producción: Costa Rica
    
    Autores: Ignacio Grané Rojas, 2019380056
             Alejandro Soto Chacón, 2019008164
    
    Bibliotecas utilizadas: PyGame 1.9.6
    """

    global EstadoCarro, Fallido

    #Características de la ventana
    pygame.display.set_caption("Test drive")
    Pantalla.fill((0,0,100))

    #Título del test drive
    Titulo = fonts.tituloTestDrive.render("Formula-E CE", 1, (255,255,255))
    Pantalla.blit(Titulo, (650,40))

    #Cuadro para mostrar todo
    pygame.draw.rect(Pantalla, (255,255,255), pygame.Rect(150,125,1240,600), 0)

    #Colores
    Blanco, Negro = (255, 255, 255), (0, 0, 0)
    FlechaSuelta, FlechaPresionada = (200, 200, 200), (123, 123, 123)

    #Retroceso 
    def atras():
        desconectar()
        main.cambiar_a(menu)

    #Botón para regresar
    main.boton_textual(25, 25, 100, 60, 'Atrás', atras, fonts.atras, Blanco)

    Car = files.Cars[files.LastYear]
    if not Conectado and not Fallido:
        EstadoCarro = Car['status']
    
    # Estado de conexión
    Centrado = True
    if EstadoCarro == 'reparations' or ((Conectado or Fallido) and EstadoCarro == 'discharged'):
        Texto = 'Carro no disponible'
        Color = (255, 255, 0)

        Fallido = True
        main.quitar_temporizador()
    elif Conectado and Fallido:
        Color = (255, 0, 0)
        Texto = 'Conexión perdida'
    elif Conectado:
        Color = (0, 255, 0)
        Texto = 'Conectado'
        Centrado = False
    elif Fallido:
        Color = (255, 0, 0)
        Texto = 'Conexión fallida'
    else:
        Color = (0, 0, 255)
        Texto = 'Conectando...'

    Estado = fonts.productoTestDrive.render(Texto, 1, Color)
    Posicion = (770 - Estado.get_width() // 2, 425 - Estado.get_height() // 2) if Centrado else (200, 700)
    Pantalla.blit(Estado, Posicion)

    #Se muestra el test drive propiamente solamente si la
    #conexión se encuentra activa y no ha fallado.
    if Conectado and not Fallido:
        Pantalla.blit(pygame.transform.scale(files.image(files.Seasons[files.LastYear]['logo']), (133, 167)), (160, 450))
        Pantalla.blit(fonts.patrocinador.render('Snowden Racing', 1, (0, 0, 0)), (200, 650))
        pygame.draw.line(Pantalla, (0,0,0), (700,180), (857,180), 2)

        Pilots = files.Pilots[files.LastYear]
        Pantalla.blit(files.image(Pilots[0]['image']), (320,135))
        Pantalla.blit(files.image(Pilots[1]['image']), (480,135))

        Vehiculo = fonts.productoTestDrive.render(Car['model'], 1, (0,0,0))
        Pantalla.blit(Vehiculo, (700,150))

        Pantalla.blit(files.image('luna.png' if NivLuz else 'sol.png'), (160,135)) #Mostrar el nivel de luz

        #Nivel de batería
        if NivBateria > 70:
            Bateria = files.image('bateriallena.png') 
        elif NivBateria > 50:
            Bateria = files.image('bateriamedia.png')
        else: # 50 >= NivBateria > 30
            Bateria = files.image('bateriavacia.png')

        Pantalla.blit(Bateria, (1280,135)) #Mostrar el porcentaje de batería
        Pantalla.blit(fonts.nombreAbout.render(str(NivBateria) + '%', 1, (0, 0, 0)), (1310, 210))

        #Datos del piloto
        Piloto = Pilots[PitSeleccionado - 1]
        NomPiloto = fonts.pilotoTestDrive.render('Piloto: ' + Piloto['name'] + ' ' + Piloto['surname'], 1, (0, 0, 0))
        NacPiloto1 = fonts.pilotoTestDrive.render("Nacionalidad: ",1,(0,0,0))
        NacPiloto2 = files.image(Piloto['nationality'])

        pygame.draw.rect(Pantalla, (255,255,255), pygame.Rect(630,230,325,80),0) #Reemplaza el nombre del piloto por uno nuevo
        Pantalla.blit(NomPiloto, (630,230)) #Muestra el nombre del piloto
        Pantalla.blit(NacPiloto1, (630,270)) #Muestra la nacionalidad del piloto
        Pantalla.blit(NacPiloto2, (800,265)) #Muestra la nacionalidad del piloto

        def cambiar_piloto(Piloto):
            global PitSeleccionado

            PitSeleccionado = Piloto
            main.cambiar_a(testdrive)

        # Botones para cambiar de piloto
        main.registrar_boton(pygame.Rect(320, 135, 133, 167), lambda _: None, lambda: cambiar_piloto(1), None, None)
        main.registrar_boton(pygame.Rect(480, 135, 133, 167), lambda _: None, lambda: cambiar_piloto(2), None, None)

        if Potencia > 0:
            if Aceleracion > 0:
                Texto = 'Acelerando hacia adelante'
            elif Aceleracion < 0:
                Texto = 'Desacelerando hacia adelante'
            else:
                Texto = 'Hacia adelante'
        elif Potencia < 0:
            if Aceleracion > 0:
                Texto = 'Desacelerando hacia atrás'
            elif Aceleracion < 0:
                Texto = 'Acelerando hacia atrás'
            else:
                Texto = 'Hacia atrás'
        else:
            Texto = 'Detenido'

        Pantalla.blit(fonts.default.render(Texto, 1, (0, 0, 0)), (650, 660))

        # Botones de control de luces
        def boton_luz(Y, Etiqueta, ID):
            Etiqueta += ': ' + ('on' if Luces.get(ID, False) else 'off')
            main.boton_textual(350, Y, 180, 40, Etiqueta, lambda: cambiar_luces(ID), fonts.default, Negro, Negro, Blanco)

        boton_luz(330, 'Frontales', control.FRONT_LIGHTS)
        boton_luz(380, 'Traseras', control.BACK_LIGHTS)
        boton_luz(430, 'Der. fron.', control.FRONT_RIGHT_LIGHTS)
        boton_luz(480, 'Izq. fron.', control.FRONT_LEFT_LIGHTS)
        boton_luz(530, 'Der. tras.', control.BACK_RIGHT_LIGHTS)
        boton_luz(580, 'Izq. tras.', control.BACK_LEFT_LIGHTS)

        Pantalla.blit(files.image('index.jpg'), (650,360))
        for Luz, Estado in Luces.items():
            if Estado:
                pass#Pantalla.blit(files.image('Carros/plano' + Luz + '.png'), (650,360))
    
        PWM = fonts.default.render(str(Potencia), 1, (0, 0, 0))
        Pantalla.blit(PWM, (855,520))

        main.recibir_teclas(tecla)

    # Se conecta al servidor al abrir la vista
    if not Conectado and not Fallido:
        pygame.display.flip()
        conectar()

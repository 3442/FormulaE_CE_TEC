# Para la interfaz de usuario
import pygame

# Para volver a la tabla
import tabla

# Bucle de eventos
import main

# Fuentes
import fonts

# Lectura y escritura de archivos
import files

# Para redibujarse
import editcar

def inspect(NewSeason):
    global Car, Season
    global BrandField, ModelField, EfficiencyField, BatteriesField, CellsField, TensionField, ConsumptionField, WeightField

    Season = NewSeason
    Car = files.Cars[Season].copy()

    BrandField = [Car['brand']]
    ModelField = [Car['model']]
    EfficiencyField = [str(Car['efficiency'])]
    BatteriesField = [str(Car['batteries'])]
    CellsField = [str(Car['cellsPerBattery'])]
    TensionField = [str(Car['batteryTension'])]
    ConsumptionField = [str(Car['motorConsumption'])]
    WeightField = [str(Car['weight'])]

def guardar():
    Car['brand'] = BrandField[0]
    Car['model'] = ModelField[0]
    Car['efficiency'] = float(EfficiencyField[0])
    Car['batteries'] = int(BatteriesField[0])
    Car['cellsPerBattery'] = int(CellsField[0])
    Car['batteryTension'] = float(TensionField[0])
    Car['motorConsumption'] = float(ConsumptionField[0])
    Car['weight'] = float(WeightField[0])

    files.Cars[Season].update(Car)

    files.update_car(Season)
    main.cambiar_a(tabla)

def cambiar_imagen():
    Car['image'] = files.select_image()
    main.cambiar_a(editcar)

def siguiente_estado():
    Viejo = Car['status']
    if Viejo == 'available':
        Nuevo = 'discharged'
    elif Viejo == 'discharged':
        Nuevo = 'reparations'
    elif Viejo == 'reparations':
        Nuevo = 'available'

    Car['status'] = Nuevo
    main.cambiar_a(editcar)

def mostrar(Pantalla):
    Pantalla.fill((100, 100, 100))

    main.boton_textual(50, 50, 80, 30, 'Descartar', lambda: main.cambiar_a(tabla), fonts.atras, pygame.Color('lightskyblue2'))
    main.boton_textual(250, 50, 80, 30, 'Guardar', guardar, fonts.atras, pygame.Color('lightskyblue2'))

    Imagen = files.image(Car['image'])
    Pantalla.blit(Imagen, (50, 100))
    main.registrar_boton(pygame.Rect(50, 100, Imagen.get_width(), Imagen.get_height()), lambda _: None, cambiar_imagen, None, None)

    Estado = Car['status']
    if Estado == 'available':
        Texto = 'Disponible'
    elif Estado == 'discharged':
        Texto = 'Descargado'
    elif Estado == 'reparations':
        Texto = 'En reparaciones'

    main.boton_textual(500, 300, 200, 40, Texto, siguiente_estado, ColorTexto = (255, 255, 255))

    Pantalla.blit(fonts.temporadaMenu.render('Marca', 1, (255, 255, 255)), (50, 400))
    main.cuadro_edicion(300, 400, 200, 20, BrandField)

    Pantalla.blit(fonts.temporadaMenu.render('Modelo', 1, (255, 255, 255)), (50, 450))
    main.cuadro_edicion(300, 450, 200, 20, ModelField)

    Pantalla.blit(fonts.temporadaMenu.render('Eficiencia', 1, (255, 255, 255)), (50, 500))
    main.cuadro_edicion(300, 500, 50, 20, EfficiencyField)

    Pantalla.blit(fonts.temporadaMenu.render('Baterías', 1, (255, 255, 255)), (50, 550))
    main.cuadro_edicion(300, 550, 50, 20, BatteriesField)

    Pantalla.blit(fonts.temporadaMenu.render('Pilas por batería', 1, (255, 255, 255)), (50, 600))
    main.cuadro_edicion(300, 600, 50, 20, CellsField)

    Pantalla.blit(fonts.temporadaMenu.render('Tensión por pila', 1, (255, 255, 255)), (50, 650))
    main.cuadro_edicion(300, 650, 50, 20, TensionField)

    Pantalla.blit(fonts.temporadaMenu.render('Consumo de motores', 1, (255, 255, 255)), (50, 700))
    main.cuadro_edicion(300, 700, 50, 20, ConsumptionField)

    Pantalla.blit(fonts.temporadaMenu.render('Peso', 1, (255, 255, 255)), (50, 750))
    main.cuadro_edicion(300, 750, 50, 20, ConsumptionField)

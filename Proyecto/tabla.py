# Para mostrar la tabla e interactuar con el usuario
import pygame

# Para regresar el menú principal
import menu

# Fuentes de texto
import fonts

# Para refrescar la misma tabla
import tabla

# Para interactuar con el sistema de eventos
import main

# Para cargar imágenes y datos de la escudería
import files

# Para editar pilotos
import editpilot

# Para editar carros
import editcar

# Por defecto se muestra la tabla de pilotos
Categoria = 1

OrdenarPorREP = False
Descendente = False

def ordenar_rgp():
    global OrdenarPorREP, Descendente

    if OrdenarPorREP:
        OrdenarPorREP = False
        Descendente = False
    else:
        Descendente = not Descendente

    main.cambiar_a(tabla)

def ordenar_rep():
    global OrdenarPorREP, Descendente

    if OrdenarPorREP:
        Descendente = not Descendente
    else:
        OrdenarPorREP = True
        Descendente = False

    main.cambiar_a(tabla)

def invertir_ordenamiento():
    global Descendente

    Descendente = not Descendente
    main.cambiar_a(tabla)

# Selection sort
def ordenar(Lista, CriterioBase):
    Criterio = (lambda A, B: not CriterioBase(A, B)) if Descendente else CriterioBase

    for I in range(len(Lista) - 1):
        Optimo = I
        for J in range(I + 1, len(Lista)):
            if Criterio(Lista[J], Lista[Optimo]):
                Optimo = J

        Lista[I], Lista[Optimo] = Lista[Optimo], Lista[I]

def nueva_temporada():
    Current = files.LastYear
    Next = Current + 1
    files.begin_season(Next)

    files.Sponsors[Next] = files.Sponsors[Current].copy()
    files.update_sponsors(Next)

    files.Seasons[Next] = files.Seasons[Current].copy()
    files.update_season(Next)

    files.Cars[Next] = files.Cars[Current].copy()
    files.Cars[Next]['brand'] = 'Genérico'
    files.Cars[Next]['model'] = str(Next)
    files.update_car(Next)

    PilotA, PilotB = files.Pilots[Current]

    PilotA = PilotA.copy()
    PilotA['name'] = 'Piloto 1'
    PilotA['surname'] = str(Next)

    PilotB = PilotB.copy()
    PilotB['name'] = 'Piloto 2'
    PilotB['surname'] = str(Next)

    files.Pilots[Next] = (PilotA, PilotB)
    files.update_pilot(Next, 0)
    files.update_pilot(Next, 1)

    files.LastYear = Next
    files.update_year()

    main.cambiar_a(tabla)

def mostrar(Pantalla):
    """
    Instituto Tecnológico de Costa Rica
    Escuela de Ingeniería en Computadores
    CE1102 Taller de Programación, Grupo 2
    Prof. Milton Villegas Lemus
    Proyecto III, I semestre 2019

    Nombre del programa: Tablas de posiciones de pilotos y carros
    Fecha de última modificación: 2019-06-04
    Versión del lenguaje: Python 3.7.6
    Versión del programa: 1.1
    País de producción: Costa Rica

    Autores: Ignacio Grané Rojas, 2019380056
             Alejandro Soto Chacón, 2019008164

    Bibliotecas utilizadas: PyGame 1.9.6
    """

    pygame.display.set_caption("Tabla de posiciones")
    Pantalla.fill((75,75,75))

    # Para cambiar la categoría se altera la variable global
    # y luego se refresca la vista
    def cambiar_categoria(Nueva):
        global Categoria

        Categoria = Nueva
        main.cambiar_a(tabla)

    # Botones de la barra superior
    main.boton_textual(25, 75, 100, 50, 'Atrás', lambda: main.cambiar_a(menu), fonts.atras, (255, 255, 255))
    main.boton_textual(500, 30, 250, 60, 'Pilotos', lambda: cambiar_categoria(1), fonts.categoriaTabla, (120, 120, 120))
    main.boton_textual(800, 30, 250, 60, 'Vehículos', lambda: cambiar_categoria(2), fonts.categoriaTabla, (120, 120, 120))

    # Categoría de pilotos
    if Categoria == 1:
        pygame.draw.rect(Pantalla, (255,255,255), pygame.Rect(200,120,1150,38 + 84 * (files.LastYear - 2014)), 0)
        pygame.draw.rect(Pantalla, (50,50,50), pygame.Rect(200,120,1150,35), 0)
        
        # Títulos de columna
        Posicion = fonts.tituloTabla.render("Pos", 1, (255,255,255))
        Pantalla.blit(Posicion, (245,125))
        Piloto = fonts.tituloTabla.render("Piloto", 1, (255,255,255))
        Pantalla.blit(Piloto, (290,125))
        Edad = fonts.tituloTabla.render("Edad", 1, (255,255,255))
        Pantalla.blit(Edad, (480,125))
        Nacionalidad = fonts.tituloTabla.render("Nacionalidad", 1, (255,255,255))
        Pantalla.blit(Nacionalidad, (540,125))
        Temporada = fonts.tituloTabla.render("Temporada", 1, (255,255,255))
        Pantalla.blit(Temporada, (700,125))
        CantComp = fonts.tituloTabla.render("Cantidad de competencias", 1, (255,255,255))
        Pantalla.blit(CantComp, (820,125))

        TituloRGP = fonts.tituloTabla.render("RGP", 1, (255,255,255))
        Pantalla.blit(TituloRGP, (1130,125))
        main.registrar_boton(pygame.Rect(1130, 125, TituloRGP.get_width(), TituloRGP.get_height()), lambda _: None, ordenar_rgp, None, None)

        TituloREP = fonts.tituloTabla.render("REP", 1, (255,255,255))
        Pantalla.blit(TituloREP, (1250,125))
        main.registrar_boton(pygame.Rect(1250, 125, TituloREP.get_width(), TituloREP.get_height()), lambda _: None, ordenar_rep, None, None)

        def boton_editar(AreaBoton, Season, PilotID):
            def al_presionar():
                editpilot.inspect(Season, PilotID)
                main.cambiar_a(editpilot)

            main.registrar_boton(AreaBoton, lambda _: None, al_presionar, None, None)

        SortedPilots = []
        for Season in range(2015, files.LastYear + 1):
            for PilotID in range(0, 2):
                Pilot = files.Pilots[Season][PilotID]

                RGP = Pilot['distinguished'] / (Pilot['participated'] - Pilot['abandoned']) * 100
                REP = Pilot['won'] / (Pilot['participated'] - Pilot['abandoned']) * 100

                SortedPilots.append((Season, PilotID, RGP, REP))

        CampoComparacion = 3 if OrdenarPorREP else 2
        ordenar(SortedPilots, lambda A, B: A[CampoComparacion] < B[CampoComparacion])

        # Se enumeran ambos pilotos de cada temporada
        for Position, Data in enumerate(SortedPilots):
            Season, PilotID, RGP, REP = Data
            Offset = 42 * Position
            Pilot = files.Pilots[Season][PilotID]

            # Líneas para separar los datos de los piltos
            pygame.draw.line(Pantalla, (200,200,200), (200, 198 + Offset), (1350, 198 + Offset), 2)

            # Texto de posición del piloto
            PositionName = fonts.datosTabla.render(str(Position + 1), 1, (0, 0, 0))
            Pantalla.blit(PositionName, (255, 160 + Offset))

            # Se muestran los datos del piloto
            Pantalla.blit(pygame.transform.scale(files.image(Pilot['image']), (27, 35)), (290, 160 + Offset))
            Names = fonts.pilotoTabla.render(Pilot['surname'] + ', ' + Pilot['name'], 1, (0, 0, 0))
            Pantalla.blit(Names, (325, 165 + Offset))
            Age = fonts.datosTabla.render(str(Pilot['age']), 1, (0,0,0))
            Pantalla.blit(Age, (487, 160 + Offset))
            Pantalla.blit(files.image(Pilot['nationality']), (565, 160 + Offset))
            Pantalla.blit(fonts.datosTabla.render(str(Season), 1, (0, 0, 0)), (715, 160 + Offset))
            Participated = fonts.datosTabla.render(str(Pilot['participated']), 1, (0, 0, 0))
            Pantalla.blit(Participated, (900, 160 + Offset))
            Pantalla.blit(fonts.datosTabla.render(str(int(RGP)), 1, (0, 0, 0)), (1135, 160 + Offset))
            Pantalla.blit(fonts.datosTabla.render(str(int(REP)), 1, (0, 0, 0)), (1255, 160 + Offset))

            EditButtonArea = pygame.Rect(205,160 + Offset,35,30)
            pygame.draw.rect(Pantalla, (200,200,200), EditButtonArea, 0)
            boton_editar(EditButtonArea, Season, PilotID)

    # Categoría de carros
    elif Categoria == 2:
        main.boton_textual(1100, 30, 250, 60, 'Agregar', nueva_temporada, fonts.categoriaTabla, (120, 120, 120))

        pygame.draw.rect(Pantalla, (255,255,255), pygame.Rect(300,120,950,30 + 70 * (files.LastYear - 2014)), 0)
        pygame.draw.rect(Pantalla, (50,50,50), pygame.Rect(300,120,950,35), 0)

        # Títulos de columna
        Posicion = fonts.tituloTabla.render("Pos", 1, (255,255,255))
        Pantalla.blit(Posicion, (345,125))
        Equipo = fonts.tituloTabla.render("Equipo", 1, (255,255,255))
        Pantalla.blit(Equipo, (390,125))
        Modelo = fonts.tituloTabla.render("Modelo", 1, (255,255,255))
        Pantalla.blit(Modelo, (550,125))
        Marca = fonts.tituloTabla.render("Marca", 1, (255,255,255))
        Pantalla.blit(Marca, (740,125))
        Temporada = fonts.tituloTabla.render("Temporada", 1, (255,255,255))
        Pantalla.blit(Temporada, (900,125))

        TituloEficiencia = fonts.tituloTabla.render("Eficiencia", 1, (255,255,255))
        Pantalla.blit(TituloEficiencia, (1100,125))
        main.registrar_boton(pygame.Rect(1100, 125, TituloEficiencia.get_width(), TituloEficiencia.get_height()), lambda _: None, invertir_ordenamiento, None, None)

        def boton_editar(AreaBoton, Season):
            def al_presionar():
                editcar.inspect(Season)
                main.cambiar_a(editcar)

            main.registrar_boton(AreaBoton, lambda _: None, al_presionar, None, None)

        SortedCars = [(Season, files.Cars[Season]['efficiency']) for Season in range(2015, files.LastYear + 1)]
        ordenar(SortedCars, lambda A, B: A[1] < B[1])

        # Se enumeran los carros de cada temporada
        for Position, Data in enumerate(SortedCars):
            Season, Efficiency = Data
            Offset = 70 * Position
            Car = files.Cars[Season]

            # Líneas para separar los datos de los piltos
            pygame.draw.line(Pantalla, (200,200,200), (300, 220 + Offset), (1250, 220 + Offset), 2)

            # Posiciones
            Position = fonts.datosTabla.render(str(Position + 1), 1, (0,0,0))
            Pantalla.blit(Position, (355, 170 + Offset))

            # Se muestran los datos del carro
            Pantalla.blit(pygame.transform.scale(files.image(Car['image']), (180, 60)), (370, 160 + Offset))
            Modelo = fonts.vehiculoTabla.render(Car['model'], 1, (0,0,0))
            Pantalla.blit(Modelo, (550, 170 + Offset))
            Marca = fonts.vehiculoTabla.render(Car['brand'], 1, (0,0,0))
            Pantalla.blit(Marca, (740, 170 + Offset))
            Temporada = fonts.vehiculoTabla.render(str(Season), 1, (0,0,0))
            Pantalla.blit(Temporada, (920, 170 + Offset))
            Eficiencia = fonts.vehiculoTabla.render(str(round(Efficiency, 2)), 1, (0,0,0))
            Pantalla.blit(Eficiencia, (1130, 170 + Offset))

            EditButtonArea = pygame.Rect(305,175 + Offset,35,25)
            pygame.draw.rect(Pantalla, (200,200,200), EditButtonArea, 0)
            boton_editar(EditButtonArea, Season)

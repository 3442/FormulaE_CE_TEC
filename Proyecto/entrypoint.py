# Este archivo es el punto de inicio del programa.
# Simplemente ejecuta el bucle principal con el menú
# principal como vista inicial. Se necesita esto para
# evitar ciclos de importación entre main y todos los
# demás módulos.

import main
import menu

main.bucle_principal(menu)

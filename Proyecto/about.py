#Bibliotecas
import pygame

#Archivos
import menu #Menú principal
import main #Funciones del main
import fonts #Fuentes
import files #Cargar imágenes

def mostrar(Pantalla):
    pygame.display.set_mode((665,480))
    pygame.display.set_caption("About") # Nombre de ventana
    Pantalla.fill((100, 100, 100)) # Color de fondo

    RecFoto1 = pygame.Rect(398, 48, 104, 135) # Marco de la primera foto
    RecFoto2 = pygame.Rect(508, 48, 104, 135) # Marco de la segunda foto
    ColorTexto = pygame.Color('lightskyblue3')

    MarcoFoto1 = pygame.draw.rect(Pantalla, ColorTexto, RecFoto1, 3)
    MarcoFoto2 = pygame.draw.rect(Pantalla, ColorTexto, RecFoto2, 3)

    ListaDatos = ["Ingeniería en Computadores", "Taller de programación - Grupo 2",
                  "Profesor: Milton Villegas Lemus", "Desarrollado en Costa Rica", "Versión 1.0", "2019"]

    ListaAutores = ["Ignacio Grané Rojas. 2019380056", "Alejandro Soto Chacón. 2019008164"]

    Y = 50
    for Dato in ListaDatos:
        label = fonts.nombreAbout.render(Dato, 1, ColorTexto)
        Pantalla.blit(label, (50, Y))
        Y += 35

    Y += 60
    for Dato in ListaAutores:
        label = fonts.nombreAbout.render(Dato,1,ColorTexto)
        Pantalla.blit(label, (50, Y))
        Y += 35

    Pantalla.blit(files.image('Autor1.png'), (400, 50)) # Imagen del autor 1
    Pantalla.blit(files.image('Autor2.png'), (510, 50)) # Imagen del autor 2

    main.boton_textual(50, 400, 80, 30, 'Atrás', lambda: main.cambiar_a(menu), fonts.atras, pygame.Color('lightskyblue2'))

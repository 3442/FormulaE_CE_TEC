# Para la interfaz de usuario
import pygame

# Para volver a la tabla
import tabla

# Bucle de eventos
import main

# Fuentes
import fonts

# Lectura y escritura de archivos
import files

# Para redibujarse
import editpilot

def inspect(NewSeason, NewPilotID):
    global Pilot, Season, PilotID
    global NameField, SurnameField, AgeField, ParticipatedField, FailedField, AbandonedField, DistinguishedField, WonField

    Season, PilotID = NewSeason, NewPilotID
    Pilot = files.Pilots[Season][PilotID].copy()

    NameField = [Pilot['name']]
    SurnameField = [Pilot['surname']]
    AgeField = [str(Pilot['age'])]
    ParticipatedField = [str(Pilot['participated'])]
    FailedField = [str(Pilot['failed'])]
    AbandonedField = [str(Pilot['abandoned'])]
    DistinguishedField = [str(Pilot['distinguished'])]
    WonField = [str(Pilot['won'])]

def guardar():
    Pilot['name'] = NameField[0]
    Pilot['surname'] = SurnameField[0]
    Pilot['age'] = int(AgeField[0])
    Pilot['participated'] = int(ParticipatedField[0])
    Pilot['failed'] = int(FailedField[0])
    Pilot['abandoned'] = int(AbandonedField[0])
    Pilot['distinguished'] = int(DistinguishedField[0])
    Pilot['won'] = int(WonField[0])

    files.Pilots[Season][PilotID].update(Pilot)

    files.update_pilot(Season, PilotID)
    main.cambiar_a(tabla)

def cambiar_imagen():
    Pilot['image'] = files.select_image()
    main.cambiar_a(editpilot)

def cambiar_pais():
    Pilot['nationality'] = files.select_image()
    main.cambiar_a(editpilot)

def mostrar(Pantalla):
    Pantalla.fill((100, 100, 100))

    main.boton_textual(50, 50, 80, 30, 'Descartar', lambda: main.cambiar_a(tabla), fonts.atras, pygame.Color('lightskyblue2'))
    main.boton_textual(250, 50, 80, 30, 'Guardar', guardar, fonts.atras, pygame.Color('lightskyblue2'))

    Imagen = files.image(Pilot['image'])
    Pantalla.blit(Imagen, (50, 150))
    main.registrar_boton(pygame.Rect(50, 150, Imagen.get_width(), Imagen.get_height()), lambda _: None, cambiar_imagen, None, None)

    Pais = files.image(Pilot['nationality'])
    Pantalla.blit(Pais, (300, 150))
    main.registrar_boton(pygame.Rect(300, 150, Pais.get_width(), Pais.get_height()), lambda _: None, cambiar_pais, None, None)

    Pantalla.blit(fonts.temporadaMenu.render('Nombre', 1, (255, 255, 255)), (50, 400))
    main.cuadro_edicion(150, 400, 200, 20, NameField)

    Pantalla.blit(fonts.temporadaMenu.render('Apellidos', 1, (255, 255, 255)), (50, 450))
    main.cuadro_edicion(150, 450, 200, 20, SurnameField)

    Pantalla.blit(fonts.temporadaMenu.render('Edad', 1, (255, 255, 255)), (50, 500))
    main.cuadro_edicion(150, 500, 50, 20, AgeField)

    Pantalla.blit(fonts.temporadaMenu.render('Participó en', 1, (255, 255, 255)), (50, 600))
    main.cuadro_edicion(200, 600, 50, 20, ParticipatedField)

    Pantalla.blit(fonts.temporadaMenu.render('Falló en', 1, (255, 255, 255)), (50, 650))
    main.cuadro_edicion(200, 650, 50, 20, FailedField)

    Pantalla.blit(fonts.temporadaMenu.render('Abandonó en', 1, (255, 255, 255)), (50, 700))
    main.cuadro_edicion(200, 700, 50, 20, AbandonedField)

    Pantalla.blit(fonts.temporadaMenu.render('Destacó en', 1, (255, 255, 255)), (50, 750))
    main.cuadro_edicion(200, 750, 50, 20, DistinguishedField)

    Pantalla.blit(fonts.temporadaMenu.render('Ganó en', 1, (255, 255, 255)), (50, 800))
    main.cuadro_edicion(200, 800, 50, 20, WonField)

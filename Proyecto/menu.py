# Se utiliza para respectos de la interfaz de usuario
import pygame

# Para cambiar a la vista de about
import about

# Para cambiar a la vista de tabla
import tabla

# Para cambiar a la vista de test drive
import testdrive

# Interacciones con el sistema de eventos
import main

# Fuentes de texto
import fonts

# Se debe importar a sí mismo para poder refrescar
# la vista luego de ciertos cambios
import menu

# Cargado de imágenes y otros datos
import files

ContAnnio = files.LastYear

def mostrar(Pantalla):
    """
    Instituto Tecnológico de Costa Rica
    Escuela de Ingeniería en Computadores
    CE1102 Taller de Programación, Grupo 2
    Prof. Milton Villegas Lemus
    Proyecto III, I semestre 2019
    
    Nombre del programa: Menú principal de la aplicación
    Fecha de última modificación: 05/06/2019
    Versión del lenguaje: Python 3.7.6
    Versión del programa: 1.1
    País de producción: Costa Rica
    
    Autores: Ignacio Grané Rojas, 2019380056
             Alejandro Soto Chacón, 2019008164
    
    Bibliotecas utilizadas: PyGame 1.9.6, Tkinter 3.7
    """

    pygame.display.set_mode((1536,864))
    # Variables y valores de la pantalla
    pygame.display.set_caption("Ventana principal")
    Pantalla.fill((100, 100, 100))

    Pantalla.blit(fonts.tituloMenu.render("Snowden", 1, (255,255,255)), (698, 16)) #Mostrar snowden
    Pantalla.blit(fonts.tituloMenu.render("Racing", 1, (255,255,255)), (698, 58)) #Mostrar racing

    # Colores
    ColorTexto = pygame.Color('lightskyblue3') #Celeste

    # Para evitar que un número se vea por detrás de otro en la temporada
    pygame.draw.rect(Pantalla, (100,100,100), pygame.Rect(698, 144, 170, 30), 0)
    NumTemporada = fonts.temporadaMenu.render("Temporada "+str(ContAnnio),1,(255,255,255)) #Escribe la temporada
    Pantalla.blit(NumTemporada, (698, 144)) #Muestra la temporada

    # Para evitar que un logo se vea detrás de otro
    pygame.draw.rect(Pantalla, (100,100,100), pygame.Rect(590,17,100,97), 0)
    LogoNuevo = pygame.transform.scale(files.image(files.Seasons[files.LastYear]['logo']), (100,97))
    Pantalla.blit(LogoNuevo, (591, 17))

    # Cuadro donde se muestra toda la información: Los pilotos, los carros, entre otros
    CuadroInfo = pygame.Rect(200,200,1140,500)
    pygame.draw.rect(Pantalla, (255,255,255), CuadroInfo, 0) #Cuadro en blanco

    Carro = files.Cars[ContAnnio]

    # Se cargan patrocinadores, logo de escudería de la temporada en cuestión, carro y pilotos
    Pats = [files.image(Path) for Path in files.Sponsors[ContAnnio]]
    Escuderia = LogoNuevo if ContAnnio == files.LastYear else files.image(files.Seasons[ContAnnio]['logo'])
    ImagenCarro = files.image(Carro['image'])
    Piloto1, Piloto2 = (files.image(Pilot['image']) for Pilot in files.Pilots[ContAnnio])

    Victorias = sum(Pilot['won'] for Season in range(2015, files.LastYear + 1) for Pilot in files.Pilots[Season])
    Participado = sum(Pilot['participated'] for Season in range(2015, files.LastYear + 1) for Pilot in files.Pilots[Season])
    IGE = Victorias / Participado * 100

    # Cuadro para mostrar logo y nombre de escudería #Cuadro Logo
    pygame.draw.rect(Pantalla, (200,200,200), pygame.Rect(210,210,350,168), 0)

    # Muestra el logo de la temporada
    Pantalla.blit(Escuderia, (215, 242))
    pygame.draw.rect(Pantalla, (200,200,200), pygame.Rect(977,210,350,168), 0)
    
    # Muestra el IGE obtenido en la temporada
    Pantalla.blit(fonts.datosMenu.render("IGE: " + str(round(IGE, 2)) + "%", 1, (255,255,255)),(1050,260))

    # Nombre de la escudería
    Pantalla.blit(fonts.tituloMenu.render("Snowden", 1, (255,255,255)), (320,235))
    Pantalla.blit(fonts.tituloMenu.render("Racing", 1, (255,255,255)), (320,280))
    Pantalla.blit(Piloto1, (605, 210)) # Muestra el piloto 1 de la temporada
    Pantalla.blit(Piloto2, (800, 210)) # Muestra el piloto 2 de la temporada
    Pantalla.blit(ImagenCarro, (240,440))    # Muestra el vehículo de la temporada

    if Carro['status'] == 'available':
        EstadoCarro = 'Disponible'
    elif Carro['status'] == 'discharged':
        EstadoCarro = 'Descargado'
    elif Carro['status'] == 'reparations':
        EstadoCarro = 'En reparaciones'

    Pantalla.blit(fonts.temporadaMenu.render('Estado: ' + EstadoCarro, 1, (0, 0, 0)), (350, 650))

    # Patrocinadores
    pygame.draw.rect(Pantalla, (150,150,150), pygame.Rect(1002,400,300,40), 0)
    Patrocinadores = fonts.temporadaMenu.render("Patrocinadores",1,(255,255,255))
    Pantalla.blit(Patrocinadores, (1065,405))

    # Botones para cambiar a las distintas vistas y para salir de la aplicación
    main.boton_textual(200, 750, 220, 50, 'Tabla', lambda: main.cambiar_a(tabla))
    main.boton_textual(506, 750, 220, 50, 'Test drive', lambda: main.cambiar_a(testdrive))
    #main.boton_textual(506, 750, 220, 50, 'Test drive', lambda: testdrive.mostrar(Pantalla, True))
    main.boton_textual(812, 750, 220, 50, 'Acerca de', lambda: main.cambiar_a(about))
    main.boton_textual(1120, 750, 220, 50, 'Salir', lambda: main.salir())

    # Se habilita la edición del logo de escudería solo para la temporada actual
    if ContAnnio == files.LastYear:
        def redibujar_editar(Presionado):
            Editar = files.image('editar' + ('pres' if Presionado else '') + '.png')
            Pantalla.blit(Editar, (1470, 10))

        def cambiar_logo():
            files.Seasons[ContAnnio]['logo'] = files.select_image()
            files.update_season(ContAnnio)

            main.cambiar_a(menu)

        main.registrar_boton(pygame.Rect(1470, 10, 50, 50), redibujar_editar, cambiar_logo, False, True)

    def boton_temporada(X, Y, Puntos, Annio):
        def redibujar(Color):
            pygame.draw.polygon(Pantalla, Color, Puntos)

        def al_presionar():
            global ContAnnio
            ContAnnio = Annio
            main.cambiar_a(menu)

        main.registrar_boton(pygame.Rect(X, Y, 20, 18), redibujar, al_presionar, (255, 255, 255), (200, 200, 200))

    # Retroceder un año
    if ContAnnio > 2015:
        boton_temporada(670, 150, [(690,150), (690,168), (670,159)], ContAnnio - 1)
    else:
        pygame.draw.polygon(Pantalla, (100,100,100), [(690,150), (690,168), (670,159)]) #Oculta el botón para retroceder

    # Avanzar un año
    if ContAnnio < files.LastYear:
        boton_temporada(875, 150, [(875, 150), (875, 168), (895, 159)], ContAnnio + 1)
    else:
        pygame.draw.polygon(Pantalla, (100,100,100), [(875,150), (875,168), (895,159)]) #Oculta el botón para avanzar

    def boton_patrocinador(X, Y, ID):
        def al_presionar():
            files.Sponsors[ContAnnio][ID] = files.select_image()
            files.update_sponsors(ContAnnio)
            main.cambiar_a(menu)

        main.registrar_boton(pygame.Rect(X, Y, 100, 100), lambda _: None, al_presionar, None, None)

    # Se dibujan los patrocinadores
    NumeroPatrocinador = 0
    for i in range(2):
        for j in range(3):
            X, Y = 977 + j * 125, 450 + i * 120
            Pantalla.blit(Pats[NumeroPatrocinador], (X, Y))

            # Para la temporada actual únicamente se permite cambiar
            # los patrocinadores haciendo click sobre su imagen
            if ContAnnio == files.LastYear:
                boton_patrocinador(X, Y, NumeroPatrocinador)

            NumeroPatrocinador += 1

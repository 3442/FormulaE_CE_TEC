# Para cargar de imágenes a partir de rutas
import pygame

# Para el cuadro de diálogo de selección de archivo
from tkinter import Tk
from tkinter.filedialog import askopenfilename
Tk().withdraw() # Evita que se abra la interfaz de Tkinter

# Para leer y escribir información estructurada al almacenamiento secundario
import json

# Para manipulación de rutas y crear directorios
import os

# Directorios de almacenado
BASE_PATH  = 'data'
IMAGE_PATH = 'Imagenes'

ImageCache = {}

def image(Ruta):
    """
    Entradas:      Ruta de una imagen relativa a IMAGE_PATH
    Salidas:       La imagen cargada
    Restricciones: La ruta existe y corresponde a una imagen válida
    Procedimiento: Se recurre a pygame si la imagen no está en la caché,
                   o de lo contrario se obtiene de la última
    """

    Image = ImageCache.get(Ruta)
    if Image is None:
        Image = pygame.image.load(os.path.join(IMAGE_PATH, Ruta))
        ImageCache[Ruta] = Image

    return Image

def select_image(Titulo = 'Seleccionar imagen'):
    # Abre un cuadro de diálogo para seleccionar una imagen y
    # devuelve su ruta relativa a IMAGE_PATH

    return os.path.relpath(askopenfilename(title = Titulo, filetypes = (("Image files", "*.jpg"), ("Image files", "*.png"), ("Image files", "*.gif"))), IMAGE_PATH)

def load(*Path):
    """
    Entradas:      Elementos de la ruta del archivo JSON a cargar
    Salidas:       Datos cargados
    Procedimiento: Se construye la ruta final y se lee usando la biblioteca json
    """

    with open(os.path.join(BASE_PATH, *Path)) as Handle:
        return json.load(Handle)

def update(Data, *Path):
    """
    Entradas:      Datos a escribir y elementos de la ruta del archivo JSON
    Procedimiento: Se construye la ruta final y se escribe usando la biblioteca json
    """

    with open(os.path.join(BASE_PATH, *Path), 'w') as Handle:
        json.dump(Data, Handle)

# Las siguientes funciones son de conveniencia y
# simplemente actualizan sus respectivos archivos
# JSON tras ser modificados.

def update_sponsors(Season):
    update(Sponsors[Season], str(Season), 'sponsors.json')

def update_season(Season):
    update(Seasons[Season], str(Season), 'season.json')

def update_car(Season):
    update(Cars[Season], str(Season), 'car.json')

def update_pilot(Season, PilotID):
    update(Pilots[Season][PilotID], str(Season), 'pilot' + str(PilotID + 1) + '.json')

def update_year():
    update(LastYear, 'year.json')

def begin_season(Season):
    os.mkdir(os.path.join(BASE_PATH, str(Season)))

Sponsors = {}
Seasons = {}
Cars = {}
Pilots = {}
LastYear = load('year.json')

# Al iniciar, se cargan todos los archivos JSON, ya que
# estos contienen, entre otros, los pilotos, las rutas de
# imágenes, los patrocinadores y los carros.

for Season in range(2015, LastYear + 1):
    SeasonPath = str(Season)

    Sponsors[Season] = load(SeasonPath, 'sponsors.json')
    Seasons[Season] = load(SeasonPath, 'season.json')
    Cars[Season] = load(SeasonPath, 'car.json')
    Pilots[Season] = (load(SeasonPath, 'pilot1.json'), load(SeasonPath, 'pilot2.json'))
